FROM alpine:latest

RUN apk --no-cache add tor iptables netcat-openbsd dnsmasq

COPY root/ /

ENTRYPOINT ["/startup.sh"]
