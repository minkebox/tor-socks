#! /bin/sh

trap "killall tor sleep nc dnsmasq; exit" TERM INT

# Permissions
chmod 700 /var/lib/tor /etc/tor
chmod 600 /etc/tor/torrc

# Monitoring
/sbin/iptables -D OUTPUT -j TX
/sbin/iptables -D INPUT -j RX
/sbin/iptables -I OUTPUT -p tcp --sport ${PROXY_PORT} -j RX
/sbin/iptables -I INPUT -p tcp --dport ${PROXY_PORT} -j TX

# Override DNS server - we want to make sure we use an external server
echo "nameserver 1.1.1.1" > /etc/resolv.conf
/usr/bin/tor &

IFACE=${__SECONDARY_INTERFACE}
if [ "${IFACE}" = "" ]; then
  IFACE=${__DEFAULT_INTERFACE}
fi
IP=$(ip addr show dev ${IFACE} | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | head -1)
cat > /tmp/wpad.content <<__EOF__
function FindProxyForURL(url, host) {
  if (isPlainHostName(host) ||
    shExpMatch(host, "*.local") ||
    shExpMatch(host, "*.${__DOMAINNAME}") ||
    isInNet(dnsResolve(host), "10.0.0.0", "255.0.0.0") ||
    isInNet(dnsResolve(host), "172.16.0.0",  "255.240.0.0") ||
    isInNet(dnsResolve(host), "192.168.0.0",  "255.255.0.0") ||
    isInNet(dnsResolve(host), "127.0.0.0", "255.255.255.0")) return "DIRECT";
  return "SOCKS5 ${IP}:${PROXY_PORT}";
}
__EOF__
cat > /tmp/wpad.dat <<__EOF__
HTTP/1.1 200 OK
Content-Type: application/x-ns-proxy-autoconfig
Content-Length: $(stat -c %s /tmp/wpad.content)
Connection: close

$(cat /tmp/wpad.content)
__EOF__
(while true ; do nc -w 1 -l 80 < /tmp/wpad.dat; done) &


if [ "${ENABLE_WPAD}" = "true" ]; then
  cat > /etc/dnsmasq.conf <<__EOF__
user=root
no-resolv
no-negcache
host-record=wpad,wpad.${__DOMAINNAME},${IP}
__EOF__
  dnsmasq
fi

sleep 2147483647d &
wait "$!"
